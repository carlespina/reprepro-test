#!/bin/bash -xe

apt-get install -y reprepro wget2 gpg-agent procps
reprepro --version

# Set up reprepro.
# Follows https://wiki.debian.org/DebianRepository/SetupWithReprepro
# And also https://www.porcheron.info/setup-your-debianubuntu-repository-with-reprepro/

cd /root

mkdir -p /srv/reprepro
cd /srv/reprepro
mkdir conf dists incoming indices logs pool project tmp

# Generating OpenGPG keys:
# https://wiki.debian.org/DebianRepository/SetupWithReprepro#Generating_OpenPGP_keys

PASS_PHRASE="Super-save-passkey"

cat << EOF > keydetails.txt
%echo Generating a basic OpenPGP key
Key-Type: default
Subkey-Type: default
Name-Real: Testing account for signing packages
Name-Comment: Used only for testing
Name-Email: noemail@noemail.com
Expire-Date: 0
Passphrase: $PASS_PHRASE
%commit
%echo done" > keydetails.txt
EOF

gpg --batch --generate-key keydetails.txt

KEYID=$(gpg --list-secret-keys --with-subkey-fingerprint | grep -A 1 '^ssb' | grep -v '^ssb' | awk '{print $1}')
KEYGRIP=$(gpg --list-keys --with-keygrip | grep -A 1 '^sub' | awk '{print $3}' | tail -1)

echo "allow-preset-passphrase" > /root/.gnupg/gpg-agent.conf

gpg-connect-agent reloadagent /bye

cat << EOF > ~/.gnupg/gpg.conf
use-agent 
pinentry-mode loopback
EOF

/usr/lib/gnupg/gpg-preset-passphrase --passphrase "$PASS_PHRASE" --preset "$KEYGRIP"

# This should not be needed (or other gpg things done above is not needed)
echo > test
echo "$PASS_PHRASE" | gpg --passphrase-fd 0 --pinentry-mode loopback --sign test
rm test test.gpg

# Configuring reprepro
# https://wiki.debian.org/DebianRepository/SetupWithReprepro#Configuring_reprepro

cd /srv/reprepro/conf

cat << EOF > distributions
Origin: Your project name
Label: Your project name
Codename: unstable
Version: 12
Architectures: source amd64
Components: main
Description: Apt repository for project x
SignWith: $KEYID
EOF

cat << EOF > incoming
Name: default
IncomingDir: incoming
TempDir: tmp
Allow: unstable
Cleanup: on_deny on_error
Permit: unused_files
EOF

# Adding packages to the repository
# https://wiki.debian.org/DebianRepository/SetupWithReprepro#Adding_packages_to_the_repository

cd /srv/reprepro/incoming
#wget2 --content-disposition http://89.145.160.104/artifact/30/hello_2.10-2_amd64.deb/
#wget2 --content-disposition http://89.145.160.104/artifact/30/hello-dbgsym_2.10-2_amd64.deb/
#wget2 --content-disposition http://89.145.160.104/artifact/29/hello_2.10-2_amd64.changes/

#mkdir /srv/storage
cp -v /data/* .

#for file in /srv/storage/*
#do
#	ln "$file" /srv/reprepro/incoming/
#done

#ls -lRi /srv/reprepro/incoming/

# Test
reprepro -Vb /srv/reprepro export

# Add the deb file
# reprepro -V -b /srv/reprepro processincoming default /src/reprepro/incoming/hello_2.10-2_amd64.changes
reprepro -V -b /srv/reprepro processincoming default hello_2.10-2_amd64.changes

reprepro -Vb /srv/reprepro export

ls -lRi /srv/reprepro

