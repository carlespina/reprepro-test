FROM debian:bookworm-slim

ENV DEBIAN_FRONTEND noninteractive

RUN \
  apt-get -y update && \
  apt-get -y dist-upgrade && \
  apt-get -y install reprepro

COPY init.sh /
COPY data/* /data/
RUN /init.sh

CMD ["sleep", "3600"]
